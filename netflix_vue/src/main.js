import Vue from 'vue'
// import App from './App.vue'
import VueResource from 'vue-resource'

Vue.config.productionTip = false
Vue.use(VueResource)

// coloquei pra abrir o objeto Home.vue como padrao ao inves do App.vue
import Home from './paginas/Home.vue'
new Vue({
  render: h => h(Home),
}).$mount('#app')

// new Vue({
//   render: h => h(App),
// }).$mount('#app')
